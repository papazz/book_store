var pg = require('pg'),
    config = require('../config');
var c = process.env.DATABASE_URL || config.db.proto + '://' + config.db.host + ':' + config.db.port + '/' + config.db.dbname;

pg.connect(c, (err, client, done) => {

    if (err) {
        done();
        console.log(err);
        return
    }

    client.query(`CREATE TABLE IF NOT EXISTS books(  id SERIAL PRIMARY KEY
                								   , title varchar(1000) not null
                								   , asin varchar(100) not null
                                                   , source varchar(1000))`)
        .on('end', () => {
            console.log('TABLE books created.');
        });

    client.query(`CREATE TABLE IF NOT EXISTS category(id SERIAL PRIMARY KEY
                                                    , name varchar(1000) not null
                                                    , url VARCHAR(1000)
                                                    , constraint u_name_constraint UNIQUE(name))`)
        .on('end', () => {
            console.log('TABLE category created.');
        });

    client.query(`CREATE TABLE IF NOT EXISTS ranks( id SERIAL PRIMARY KEY
                    							  , book_id integer references books (id)
                    							  , cat_id integer references category (id)
                    						  	  , rank integer not null
                    							  , constraint u_bkcat_constraint UNIQUE (book_id, cat_id))
    			`)
        .on('end', () => {
            console.log('TABLE ranks created.');
            done();
            process.exit(0);
        });
});