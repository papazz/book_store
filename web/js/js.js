$(document).ready(function() {
    loadCat();
    $.ajax({
        url: "/scrap",
        success: function(data) {
            var html = ""
            data.cat.forEach((cat) => {
                html = html + "<li><a href='#' data-value='" + cat.id + "'>" + cat.name + "</a></li>";
            });
            $(".inform").text(data.books.count + " books in " + data.cat.length + " categories");
            $(".dropdown-menu").html(html);
        }
    });

    $(".scrap").on("click", () => {
        $(".btn").addClass("disabled");
        var loadData = function() {
            $.ajax({
                url: "/scrap",
                method: 'POST',
                contentType: "application/json; charset=utf-8",
                data: '{"top": 100}',
                success: () => {
                    loadCat();
                    $(".btn").removeClass("disabled");
                }
            })
        };
        loadData();
    });

    function loadCat(data) {
        $(".dropdown-menu").on("click", "li a", function() {
            a = $(this).closest("a");
            var cat_id = a.attr('data-value');
            $('.btn-lbl').text(a.text());
            $.ajax({
                url: "/scrap",
                data: "cat_id=" + cat_id,
                success: function(books) {
                    var table = "<tbody>";
                    books.forEach((book) => {
                        table += "<tr data-status='pagado'>\
                                <td> \
                                    <a href='javascript:;' class='star'> \
                                        <i class='glyphicon glyphicon-star'></i>Rank " + book.rank + "\
                                    </a> \
                                </td> \
                                <td> \
                                    <div class='media'> \
                                        <a href='#' class='pull-left'> \
                                            <img src='http://vignette3.wikia.nocookie.net/vocaloid/images/6/6a/Book_placeholder.png/revision/latest?cb=20140717130031' class='media-photo'>\
                                        </a>\
                                        <div class='media-body'>\
                                            <span class='media-meta pull-right'></span>\
                                            <h4 class='title'>\
                                                <a href='" + book.source + "'>" + book.title + "</a>\
                                                <span class='pull-right pagado'>" + book.asin + "</span>\
                                            </h4>\
                                            <p class='summary'><a href='" + book.url + "'>" + book.name + "</a></p>\
                                        </div>\
                                    </div>\
                                </td>\
                            </tr>";
                    });
                    table += "</tbody>";


                    $(".table").html(table);
                }
            });
        });
    }
})