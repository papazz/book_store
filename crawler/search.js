"use strict"

var config = require("../config"),
    cheerio = require("cheerio");

// escaping strings
var addSlashes = function(str) {
  return (str + '')
    .replace(/[\\"']/g, '\\$&')
    .replace(/\u0000/g, '\\0');
}

module.exports.scrapDetails = function(html) {
    var jQuery = cheerio.load(html);
    var book = {};
    // when crawler doing request very often Amazon service can expose page scanning and issue Captcha page
    // ignore this page
    var antibot = jQuery("title[dir=ltr]").text();
    book.recognized = false;

    if (antibot !== "Robot Check") {
        book.recognized = true;
        book.ranks = [];
        book.title = addSlashes(jQuery(config.jq.title).text());
        book.source = jQuery('link[rel=canonical]').attr('href');
        book.asin = book.source.substr(book.source.lastIndexOf("/") + 1);

        jQuery(config.jq.ranks).find('li').map((i, r) => {
            var row = jQuery(r);
            if (row.children(config.jq.cat).find('>a:first-child').text() != "Books") {
                var rank = {};
                rank.place = row.children(config.jq.rplace).text().replace(/^#/, '');
                rank.catUrl = row.children(config.jq.cat).find('b').find('a').attr('href');
                rank.catName = addSlashes(row.children(config.jq.cat).find('b').text());
                book.ranks.push(rank);
            }
        });
    }
    
    return book
}

module.exports.scrapRef = function(html) {
    var jQuery = cheerio.load(html);
    var bookRefs = [];

    var books = jQuery(config.jq.bdetails);
    books.each((k, b) => {
        var url = jQuery(b).attr("href");
        bookRefs.push(url);
    });

    return bookRefs
}

module.exports.fixRedirectUrl = function(s) {
    var prefix = config.baseUrl;
    if (s.substr(0, prefix.length) !== prefix) {
        s = prefix + s;
    }
    return s
}