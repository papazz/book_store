'use strict'

const config = require("../config"),
    request = require("request"),
    models = require("../models"),
    search = require("./search");

module.exports.scrap = function(req, res, next) {
    var top = req.body["top"];
    var pageList = [],
        test = [];
    var maxPage = Math.floor(top / 16) + 1; //(top > 16) ? Math.floor(top/16) + Math.floor(16/(top-Math.floor(top/16)*16)): 1;
    var options = {
        url: "",
        headers: config.h,
        // proxy: config.proxy,
        gzip: config.gzip,
        timeout: 50000
    }

    for (var p = 1; p <= maxPage; p++) {
        pageList.push(config.baseUrl + config.url + p);
    }

    var promises = pageList.map(url => new Promise((resolve, reject) => {
        setTimeout(() => {
            options.url = url
            request(options, (error, resopnse, html) => {
                if (!error) {
                    resolve(search.scrapRef(html));
                } else {
                    console.log("Page request: ", error);
                }
            });
        }, Math.floor(Math.random() * config.delayLim));
    }));

    function scrapBook(bookRefs, req, res) {
        var books = bookRefs.map(purl => new Promise((resolve, reject) => {
            setTimeout(() => {
                options.url = search.fixRedirectUrl(purl);
                request(options, (error, response, html) => {
                    if (!error) {
                        resolve(search.scrapDetails(html));
                    } else {
                        console.log("Book request error: ", error);
                    }
                });
            }, Math.floor(Math.random() * config.delayLim))
        }));

        Promise.all(books)
            .then(result => {
                models.saveBooks(result);
                models.loadData(req, res);
            }).catch(reason => {
                console.log("Parse Books error: ", reason);
            })
    }

    Promise.all(promises)
        .then(books => {
            scrapBook([].concat.apply([], books), req, res);
            next();
        }).catch(reason => {
            console.log("Parse Pages error: ", reason);
        })
}

module.exports.data = function(req, res) {
    if (req.query.cat_id != undefined) {
        models.selectBooks(req, res);
    } else {
        models.loadData(req, res);
    }
}