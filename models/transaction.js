"use strict";

var config = require('../config'),
    dml = require('./dml'),
    pg = require('pg');
var c = process.env.DATABASE_URL || config.db.proto + '://' + config.db.host + ':' + config.db.port + '/' + config.db.dbname;

module.exports.saveBooks = function(req, res) {
    pg.connect(c, (err, client, done) => {
        if (err) {
            done();
            console.log(err);
            return res.status(500).json({
                success: false,
                data: err
            });
        }

        req.forEach((book) => {
            if (book.recognized) {
                dml.updateBook(client, book)
                dml.insertBook(client, book)
                    .on('row', (bookId) => {
                        book.ranks.forEach(rank => {
                            dml.insertCategory(client, rank)
                                .on('row', (catId) => {
                                    dml.updateRank(client, bookId, catId, rank)
                                    dml.insertRank(client, bookId, catId, rank)
                                        .on('end', () => {
                                            done();
                                        });
                                    done();
                                });
                        });
                        done();
                        console.log("[ == book " + book.title + ", " + book.asin + " stored ]");
                    });
                done();
            }
        });
    });
}

module.exports.loadData = function(req, res) {
    var data = {},
        result = [];
    pg.connect(c, (err, client, done) => {
        if (err) {
            done();
            console.log(err);
            res.status(500).json({
                success: false,
                data: err
            });
        }
        dml.selectAllCategory(client)
            .on('row', (row) => {
                result.push(row);
            })
            .on('end', (categories) => {
                data.cat = result;
                done();
            })
        dml.selectBooksCount(client)
            .on('row', (count) => {
                data.books = count;
            })
            .on('end', (categories) => {
                done();
                res.json(data);
            })
    });
}

module.exports.selectBooks = function(req, res) {
    var result = []
    pg.connect(c, (err, client, done) => {
        if (err) {
            done();
            console.log(err);
            res.status(500).json({
                success: false,
                data: err
            });
        }
        dml.selectBooksByCategory(client, req.query.cat_id)
            .on('row', (row) => {
                result.push(row);
            })
            .on('end', (categories) => {
                done();
                res.json(result);
            })
    });
}