// book dml
module.exports.insertBook = function(client, book) {
    return client.query("WITH a AS ( \
									  SELECT id FROM books \
    								  	WHERE asin = $2 \
    								), \
    					       b AS ( \
    					       		  INSERT INTO books (title, asin, source) \
    					                SELECT cast($1 as VARCHAR), cast($2 as VARCHAR), cast($3 as VARCHAR) \
    					                  WHERE NOT EXISTS (SELECT 1 FROM a) \
    					                RETURNING id \
    					            ) \
    					            SELECT id \
    					              FROM b \
    					            UNION ALL \
    					            SELECT id \
    					              FROM a", [book.title, book.asin, book.source])
}

module.exports.updateBook = function(client, book) {
    return client.query("UPDATE books SET title = $1, source = $3 WHERE asin = $2", [book.title, book.asin, book.source])
}

// category dml
module.exports.insertCategory = function(client, cat) {
    return client.query("WITH a AS ( \
									  SELECT id FROM category \
    								    WHERE name = $1 \
    								), \
    					      b AS ( \
    					              INSERT INTO category (name, url) \
    					                SELECT cast($1 as VARCHAR), cast($2 as VARCHAR) \
    					                  WHERE NOT EXISTS (SELECT 1 FROM a) \
    					                RETURNING id \
    					            ) \
    					            SELECT id \
    					              FROM b \
    					            UNION ALL \
    					            SELECT id \
    					              FROM a", [cat.catName, cat.catUrl])
}

// rank dml
module.exports.insertRank = function(client, book, cat, rank) {
    return client.query("INSERT INTO ranks(book_id, cat_id, rank) \
												SELECT $1, $2, $3 \
													WHERE NOT EXISTS ( SELECT 1 \
																		  FROM ranks \
																		  WHERE book_id = $1 AND cat_id = $2) RETURNING id", [book.id, cat.id, rank.place])
}

module.exports.updateRank = function(client, book, cat, rank) {
    return client.query("UPDATE ranks SET rank = $3 WHERE book_id = $1 AND cat_id = $2", [book.id, cat.id, rank.place])
}


module.exports.selectBooksByCategory = function(client, catId) {
    return client.query("SELECT * FROM books LEFT JOIN ranks ON \
												(ranks.book_id = books.id) \
											 LEFT JOIN category ON \
												(ranks.cat_id = category.id) \
										     WHERE category.id = $1 ORDER BY ranks.rank", [catId])
}

module.exports.selectAllCategory = function(client) {
    return client.query("SELECT id, name, url FROM category ORDER BY name")
}

module.exports.selectBooksCount = function(client) {
    return client.query("SELECT count(*) FROM books")
}