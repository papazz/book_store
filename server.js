var restify = require('restify'),
 	crawler = require('./crawler'),
	models = require('./models');

var server = restify.createServer({
	name: 'Scraper',	
	version: '0.0.1'
})


server.use(restify.queryParser());
server.use(restify.bodyParser({mapParams: false}));
server.get('/scrap', crawler.data);
server.post('/scrap', crawler.scrap);
server.get("/.*/", restify.serveStatic({
  directory: './web',
  default: 'index.html'
}));
module.exports = server;