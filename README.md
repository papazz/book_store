# Test application
## Prepare & Install app

```
npm install
```

Also you need install PostgreSQL and change `config/env/test.json` config file properly

## Install database Mac OSx

[Install Postgres](http://postgresapp.com/)


After all configuration prepared execute script `node db/schema.js` to execute DDL commands and create tables

## Run

```
node index.js
```

## Usage
Look up configuration file `config/env/test.json` first and set proper delayLim 
because Amazon have anti- bot, spider protection and may limit webpage access or ask captcha input,
so it will highly reduce data return.
app will start at `http://localhost:5000` by default
page have `scrap` button and choose `category` scrollable menu
	
## Model Data
### Table Books

| id |               title 							 |   asin     |                             source 										   |
|----|-----------------------------------------------|------------|----------------------------------------------------------------------------|
| 92 | Rendezvous with Rama                          | B00AHIP8ZM | http://www.amazon.com/Rendezvous-Rama-Arthur-C-Clarke-ebook/dp/B00AHIP8ZM  |
| 99 | A Human Element (The Element Trilogy Book 1)  | B00M45L8CI | http://www.amazon.com/Human-Element-The-Trilogy-Book-ebook/dp/B00M45L8CI   |
| 1  | Mind\'s Eye                                   | B00HVGGH76 | http://www.amazon.com/Minds-Eye-Douglas-E-Richards-ebook/dp/B00HVGGH76     |

`id` internal sequence and surrogate key
`title` name of a Book, may contain escaped strings
`asin` Amazon Book code
`source` link to Amazon Book page

### Table Category

| id |        name         |                                               url                                                                |
|----|---------------------|------------------------------------------------------------------------------------------------------------------|
| 1  | Men\'s Adventure    | http://www.amazon.com/gp/bestsellers/digital-text/7588730011/ref=pd_zg_hrsr_kstore_1_5_last/189-9224026-1823848  |   
| 2  | Military            | http://www.amazon.com/gp/bestsellers/digital-text/6361464011/ref=pd_zg_hrsr_kstore_2_5_last/189-9224026-1823848  |
| 3  | Medical             | http://www.amazon.com/gp/bestsellers/digital-text/2225099011/ref=pd_zg_hrsr_kstore_3_5_last/189-9224026-1823848  |
| 4  | Science Fiction     | http://www.amazon.com/gp/bestsellers/digital-text/158592011/ref=pd_zg_hrsr_kstore_3_5_last/191-8829230-0427434   |

`id` internal sequence and surrogate key
`title` name of a Category, also may contain escaped strings
`url` Amazon Category link

### Table Ranks 

| id | book\_id  | cat\_id | rank |
|----|-----------|---------|------|
|  5 |       4   |      5  |    7 |
|  6 |       4   |      4  |    7 |
| 15 |       9   |     13  |    1 |
| 16 |       9   |     14  |    6 |
|  2 |       1   |      2  |    4 |
|  3 |       1   |      3  |    5 |

`id` internal sequence and surrogate key
`book\_id` foreign key of books.id 
`cat\_id` foreign key of category.id 
`rank`  Book rank in Amazon Book Category

## Screenshots

![Alt text](https://photos-3.dropbox.com/t/2/AAAgQt-GL3CzUb_B_Y9_WGHEerxjOJyTVRZFZIpT1XMtdQ/12/40896289/png/32x32/1/_/1/2/Screenshot%202016-03-08%2014.23.04.png/EIG9nB8Y24YIIAIoAg/fO56VS40pHXDH33Zb2FljiUQD-nDLdVNKKsMpepnyEw?size_mode=5)